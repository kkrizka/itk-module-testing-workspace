# Using globaly provided gcc from /cvmfs/sft.cern.ch/lcg/contrib
# found package tbb-adcbe
# found package mysql-7d566
# found package jemalloc-8154a
# found package zeromq-df2eb
# found package pkg_config-c6baf
# found package libsodium-0b20d
# found package Boost-c319e
# found package numpy-1af8f
# found package cython-aa7c8
# found package Python-9a1bc
# found package sqlite-3c47f
# found package libffi-a3607
# found package setuptools-e5403
# found package Python-9a1bc
# found package Python-9a1bc
# found package blas-6339b
# found package setuptools-e5403
# found package Python-9a1bc
# found package bison-01341
# found package msgpackc-c4011
# found package protobuf-5052d
# found package Python-9a1bc
# found package setuptools-e5403
# found package cfitsio-e4bb8
# found package xrootd-558a9
# found package Python-9a1bc
# found package pip-0a3af
# found package wheel-0cbbc
# found package Python-9a1bc
# found package setuptools-e5403
# found package Python-9a1bc
# found package setuptools-e5403
# found package Vc-57098
# found package numpy-1af8f
# found package oracle-40bff
# found package Python-9a1bc
# found package vdt-7622c
# found package Python-9a1bc
# found package jsonmcpp-f26c3
# found package blas-6339b
# found package GSL-30ba4
# found package fftw-cd301
# found package Davix-87b18
# found package Boost-c319e
# found package gl2ps-26d26
# found package png-9c2fe
source /cvmfs/sft.cern.ch/lcg/contrib/gcc/11/x86_64-centos9/setup.sh;
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/ROOT/6.26.14/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/ROOT/6.26.14/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/ROOT/6.26.14/x86_64-centos9-gcc11-opt/lib/DistRDF:$LD_LIBRARY_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/ROOT/6.26.14/x86_64-centos9-gcc11-opt/lib/cppyy:$LD_LIBRARY_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/ROOT/6.26.14/x86_64-centos9-gcc11-opt/lib/JsMVA:$LD_LIBRARY_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/ROOT/6.26.14/x86_64-centos9-gcc11-opt/lib/JupyROOT:$LD_LIBRARY_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/ROOT/6.26.14/x86_64-centos9-gcc11-opt/lib/cppyy_backend:$LD_LIBRARY_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/ROOT/6.26.14/x86_64-centos9-gcc11-opt/lib/__pycache__:$LD_LIBRARY_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/ROOT/6.26.14/x86_64-centos9-gcc11-opt/lib/ROOT:$LD_LIBRARY_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/tbb/2020_U2/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/tbb/2020_U2/x86_64-centos9-gcc11-opt/lib/cmake:$LD_LIBRARY_PATH";
export TBB__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/tbb/2020_U2/x86_64-centos9-gcc11-opt";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/mysql/10.5.13/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/mysql/10.5.13/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/mysql/10.5.13/x86_64-centos9-gcc11-opt/lib/plugin:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/mysql/10.5.13/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/jemalloc/5.2.1/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/jemalloc/5.2.1/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/jemalloc/5.2.1/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export JEMALLOC__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/jemalloc/5.2.1/x86_64-centos9-gcc11-opt";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/zeromq/4.3.4p1/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/zeromq/4.3.4p1/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/zeromq/4.3.4p1/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/pkg_config/0.29.2/x86_64-centos9-gcc11-opt/bin:$PATH";
export PKG_CONFIG__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/pkg_config/0.29.2/x86_64-centos9-gcc11-opt";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/libsodium/1.0.18/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/libsodium/1.0.18/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export LIBSODIUM__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/libsodium/1.0.18/x86_64-centos9-gcc11-opt";
export ZEROMQ__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/zeromq/4.3.4p1/x86_64-centos9-gcc11-opt";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Boost/1.78.0/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Boost/1.78.0/x86_64-centos9-gcc11-opt/lib/cmake:$LD_LIBRARY_PATH";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/numpy/1.22.3/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/numpy/1.22.3/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PYTHONPATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/numpy/1.22.3/x86_64-centos9-gcc11-opt/lib/python3.9/site-packages:$PYTHONPATH";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/cython/0.29.28/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/cython/0.29.28/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PYTHONPATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/cython/0.29.28/x86_64-centos9-gcc11-opt/lib/python3.9/site-packages:$PYTHONPATH";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Python/3.9.12/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Python/3.9.12/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PYTHONPATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Python/3.9.12/x86_64-centos9-gcc11-opt/lib/python3.9/site-packages:$PYTHONPATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Python/3.9.12/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/sqlite/3320300/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/sqlite/3320300/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/sqlite/3320300/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export SQLITE__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/sqlite/3320300/x86_64-centos9-gcc11-opt";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/libffi/3.4.2/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/libffi/3.4.2/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/libffi/3.4.2/x86_64-centos9-gcc11-opt/lib64:$LD_LIBRARY_PATH";
export LIBFFI__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/libffi/3.4.2/x86_64-centos9-gcc11-opt";
export PYTHON__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Python/3.9.12/x86_64-centos9-gcc11-opt";
cd "/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Python/3.9.12/x86_64-centos9-gcc11-opt"
export PYTHONHOME="${PYTHON__HOME}"
export PYTHONVERSION=`python -c "from __future__ import print_function; import sys; print ('%d.%d' % (sys.version_info.major, sys.version_info.minor))"`
cd - 1>/dev/null # from /cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Python/3.9.12/x86_64-centos9-gcc11-opt
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/setuptools/57.1.0/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PYTHONPATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/setuptools/57.1.0/x86_64-centos9-gcc11-opt/lib/python3.9/site-packages:$PYTHONPATH";
export SETUPTOOLS__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/setuptools/57.1.0/x86_64-centos9-gcc11-opt";
export CYTHON__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/cython/0.29.28/x86_64-centos9-gcc11-opt";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/blas/0.3.20.openblas/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/blas/0.3.20.openblas/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/blas/0.3.20.openblas/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/blas/0.3.20.openblas/x86_64-centos9-gcc11-opt/lib/cmake:$LD_LIBRARY_PATH";
export BLAS__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/blas/0.3.20.openblas/x86_64-centos9-gcc11-opt";
export NUMPY__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/numpy/1.22.3/x86_64-centos9-gcc11-opt";
export BOOST__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Boost/1.78.0/x86_64-centos9-gcc11-opt";
cd "/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Boost/1.78.0/x86_64-centos9-gcc11-opt"
export CPLUS_INCLUDE_PATH=${BOOST__HOME}/include:$CPLUS_INCLUDE_PATH
export C_INCLUDE_PATH=${BOOST__HOME}/include:$C_INCLUDE_PATH
cd - 1>/dev/null # from /cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Boost/1.78.0/x86_64-centos9-gcc11-opt
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/bison/3.3.2/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/bison/3.3.2/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export BISON__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/bison/3.3.2/x86_64-centos9-gcc11-opt";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/msgpackc/3.2.0/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/msgpackc/3.2.0/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/msgpackc/3.2.0/x86_64-centos9-gcc11-opt/lib/cmake:$LD_LIBRARY_PATH";
export MSGPACKC__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/msgpackc/3.2.0/x86_64-centos9-gcc11-opt";
export MYSQL__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/mysql/10.5.13/x86_64-centos9-gcc11-opt";
cd "/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/mysql/10.5.13/x86_64-centos9-gcc11-opt"
export MYSQL_HOME=${MYSQL_HOME}
cd - 1>/dev/null # from /cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/mysql/10.5.13/x86_64-centos9-gcc11-opt
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/protobuf/3.18.1/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/protobuf/3.18.1/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PYTHONPATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/protobuf/3.18.1/x86_64-centos9-gcc11-opt/lib/python3.9/site-packages:$PYTHONPATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/protobuf/3.18.1/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export PROTOBUF__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/protobuf/3.18.1/x86_64-centos9-gcc11-opt";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/cfitsio/3.48/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/cfitsio/3.48/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export CFITSIO__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/cfitsio/3.48/x86_64-centos9-gcc11-opt";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/xrootd/5.4.3/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/xrootd/5.4.3/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PYTHONPATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/xrootd/5.4.3/x86_64-centos9-gcc11-opt/lib/python3.9/site-packages:$PYTHONPATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/xrootd/5.4.3/x86_64-centos9-gcc11-opt/lib64:$LD_LIBRARY_PATH";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/pip/22.0.4/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/pip/22.0.4/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PYTHONPATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/pip/22.0.4/x86_64-centos9-gcc11-opt/lib/python3.9/site-packages:$PYTHONPATH";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/wheel/0.37.1/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/wheel/0.37.1/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PYTHONPATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/wheel/0.37.1/x86_64-centos9-gcc11-opt/lib/python3.9/site-packages:$PYTHONPATH";
export WHEEL__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/wheel/0.37.1/x86_64-centos9-gcc11-opt";
export PIP__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/pip/22.0.4/x86_64-centos9-gcc11-opt";
export XROOTD__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/xrootd/5.4.3/x86_64-centos9-gcc11-opt";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Vc/1.4.3/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Vc/1.4.3/x86_64-centos9-gcc11-opt/lib/cmake:$LD_LIBRARY_PATH";
export VC__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Vc/1.4.3/x86_64-centos9-gcc11-opt";
cd "/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Vc/1.4.3/x86_64-centos9-gcc11-opt"
ROOT_INCLUDE_PATH=${VC__HOME}/include:$ROOT_INCLUDE_PATH
export ROOT_INCLUDE_PATH
cd - 1>/dev/null # from /cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Vc/1.4.3/x86_64-centos9-gcc11-opt
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/oracle/19.11.0.0.0/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/oracle/19.11.0.0.0/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/oracle/19.11.0.0.0/x86_64-centos9-gcc11-opt/lib/network:$LD_LIBRARY_PATH";
export ORACLE__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/oracle/19.11.0.0.0/x86_64-centos9-gcc11-opt";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/vdt/0.4.3/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export VDT__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/vdt/0.4.3/x86_64-centos9-gcc11-opt";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/jsonmcpp/3.10.5/x86_64-centos9-gcc11-opt/lib64:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/jsonmcpp/3.10.5/x86_64-centos9-gcc11-opt/lib64/pkgconfig:$PKG_CONFIG_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/jsonmcpp/3.10.5/x86_64-centos9-gcc11-opt/lib64/cmake:$LD_LIBRARY_PATH";
export JSONMCPP__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/jsonmcpp/3.10.5/x86_64-centos9-gcc11-opt";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/GSL/2.7/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/GSL/2.7/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/GSL/2.7/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export GSL__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/GSL/2.7/x86_64-centos9-gcc11-opt";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/fftw3/3.3.8/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/fftw3/3.3.8/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/fftw3/3.3.8/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/fftw3/3.3.8/x86_64-centos9-gcc11-opt/lib/cmake:$LD_LIBRARY_PATH";
export FFTW__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/fftw3/3.3.8/x86_64-centos9-gcc11-opt";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Davix/0.8.2/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Davix/0.8.2/x86_64-centos9-gcc11-opt/lib64:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Davix/0.8.2/x86_64-centos9-gcc11-opt/lib64/pkgconfig:$PKG_CONFIG_PATH";
export DAVIX__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/Davix/0.8.2/x86_64-centos9-gcc11-opt";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/gl2ps/1.4.0/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/png/1.6.37/x86_64-centos9-gcc11-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/png/1.6.37/x86_64-centos9-gcc11-opt/lib:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/png/1.6.37/x86_64-centos9-gcc11-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export PNG__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/png/1.6.37/x86_64-centos9-gcc11-opt";
export GL2PS__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/gl2ps/1.4.0/x86_64-centos9-gcc11-opt";
export ROOT__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/ROOT/6.26.14/x86_64-centos9-gcc11-opt";
cd "/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/ROOT/6.26.14/x86_64-centos9-gcc11-opt"
test -s $ROOT__HOME/bin/thisroot.sh && source $ROOT__HOME/bin/thisroot.sh
cd - 1>/dev/null # from /cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_11a/ROOT/6.26.14/x86_64-centos9-gcc11-opt
